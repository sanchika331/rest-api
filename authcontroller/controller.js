var jwt = require('jsonwentokens');
var bcrypt = require('bcryptjs');
var config = require('../config');
var express = require('express');
var router =  express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: false}));
var User = require('../user/Users');


router.post('/register', function (req, res) {


    var hashedpassword = bcrypt.hashSync(req.body.password, 8);


    User.create({
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword
    },
        function (err, user) {
            if (err) return res.status(500).send(" There was a problem regestering the user")

            //creating tokens

            var token = jwt.sign({id: user._id}, config.secret, {
                expiresIn: 86400 //expires in 24 hrs
            });

            res.status(200).send({auth: true, token: token});
        });

});

router.get('/me', function (req, res) {
    var token = req.headers['x-access-token'];
    if(!token) return res.status(401).send({auth: false, message:
    'No token provided'});

    jwt.verify(token, config.secret, function (err, decoded) {
        if(err) return res.status(500).send({auth: false, message:
        'Failed to authenticate'});

        res.status(200).send(decoded);

    });


});



module.exports = router;
